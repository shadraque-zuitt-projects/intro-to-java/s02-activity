package com.clave.s02activity;

import java.util.Scanner;

public class LeapYearIdentifier {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter year: \n");
        int year = Integer.parseInt(input.nextLine());

        if ((year % 4 == 0) && (year % 100 != 0) || (year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) {
            System.out.println(year + " is a leap year.");
        } else {
            System.out.println(year + " is not a leap year.");
        }

    }
}
